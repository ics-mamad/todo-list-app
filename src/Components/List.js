import React from "react";
import { FaEdit, FaTrash } from "react-icons/fa";

//This component will displaay all available items on the page

const List = ({items, removeItem, editItem}) => {
  return (
    <div className="container">
      {items.map((item) => {         //it will iterate through all items available in list
        const { id, title } = item;

        return (
          <ul className="list-group list-group-flush" key={id}>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              {title}
              <div style={{ float: "right" }}>
                <button
                  type="button"
                  className="edit-btn"
                  onClick={() => editItem(id)} //edit button for individual task

                >
                  <FaEdit />  
                </button>
                <button
                  type="button"
                  className="delete-btn"
                  onClick={() => removeItem(id)} //remove button for individual task
                >
                  <FaTrash />
                </button>
              </div>
            </li>
          </ul>
        );
      })}
    </div>
  );
};

export default List;
