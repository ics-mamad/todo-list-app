import React, { useEffect } from 'react'

// This component is for giving alert at the time of any action

const Alert = ({type,msg,removeAlert,list}) => {
    useEffect(()=>{
        const timeout = setTimeout(()=>{
            removeAlert();
        },3000)
        return ()=> clearTimeout(timeout)
    },[list])
  return (
   <p className={`alert alert-${type}`}>{msg}</p>
  )
}

export default Alert
