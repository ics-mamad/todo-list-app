import './App.css';
import { useState,useEffect } from 'react';
import List from './Components/List';
import Alert from './Components/Alert';

const getLocalStorage = () =>{ // fetching data from local storage
  let list = localStorage.getItem("list")
  if(list){
    return (list = JSON.parse(localStorage.getItem("list")))
  }else{
    return []
  }
}

function App() {
   const [name, setName] = useState("")
   const [list, setlist] = useState(getLocalStorage()) 
   const [isEditing, setisEditing] = useState(false) //it will change icon of editing either true or false
   const [editID, seteditID] = useState(null) //maitain edited IDs
   const [alert, setAlert] = useState({show: false, msg: "",type: ""}) //for giving alert for particular action

   useEffect(()=>{
    localStorage.setItem("list",JSON.stringify(list)) // set data to local storage
   },[list])

  const handleSubmit = (e) => { // this funtion will handle of submmiting tasks or add task
    e.preventDefault();
    if (!name) { //verify that textbox is not null
      showAlert(true, "danger", "Please Enter Task");
    } else if (name && isEditing) {
      const updatedList = list.map((item) =>
        item.id === editID ? { ...item, title: name } : item
      );
      setlist(updatedList);
      setName("");
      seteditID(null);
      setisEditing(false);
      showAlert(true, "success", "Value Changed Successfully");
    } else {
      showAlert(true, "success", "Item Added To The List Successfully");
      const newItem = { id: new Date().getTime().toString(), title: name };
      setlist([...list, newItem]);
    }
  };

  const showAlert = (show=false, type="", msg="") => { //alert msg funtion
    setAlert({show,type,msg})
  } 
  const removeItem = (id) => { 
    showAlert(true,"danger","Item Removed Successfully")
    setlist(list.filter((item) => item.id!==id))
  }
  const editItem = (id) => {
    const editItem = list.find((item) => item.id === id)
    setisEditing(true)
    seteditID(id)
    setName(editItem.title)
  }
  const clearList = () => {
    showAlert(true,"danger","Empty List")
    setlist([]) //clear whole list and set it as empty
  }  

  return (
    <section className='section-center'>
      <form onSubmit={handleSubmit}>
        {alert.show && <Alert {...alert} removeAlert={showAlert} list={list} />}
          <h3 style={{marginBottom: "1.5rem", textAlign:"center"}}>Todo-List App</h3>

          <div className='mb-3 form'>
            <input type='text' className='form-control' placeholder='e.g Homework' onChange={(e)=>setName(e.target.value)} value={name} />
            <button type='submit' className='smbt-btn'>
              {isEditing ?"Edit" : "Submit"}
            </button>
          </div>
      </form>
      {list.length> 0 && (
        <div style={{marginTop:"2rem"}}>
          <List items={list} removeItem={removeItem} editItem={editItem} /> 
          <div className='text-center'>
            <button className='clr-btn' onClick={clearList}>Clear Items</button>
          </div>
        </div>
      )}
    </section>
  );
}

export default App;
